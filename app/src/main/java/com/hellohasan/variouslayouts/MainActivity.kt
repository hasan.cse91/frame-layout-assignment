package com.hellohasan.variouslayouts

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_bottom.*
import kotlinx.android.synthetic.main.content_frame.*
import kotlinx.android.synthetic.main.content_top.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        doneTopLayout.setOnClickListener {
            frameLayout.visibility = View.VISIBLE
        }

        bottomDoneButton.setOnClickListener {
            frameLayout.visibility = View.VISIBLE
        }

        closeButton.setOnClickListener{
            frameLayout.visibility = View.GONE
        }
    }
}
